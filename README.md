# Pokapi API

> A small school project with VueJS

## Build Setup

To launch this project you will need [Python]('https://www.python.org/') and the [Flask]('https://flask.palletsprojects.com/en/1.1.x/installation/#install-flask') library

```bash
# install flask with pip
$ pip install Flask
```

## Start project

To run API server

```bash
# It listen on localhost:8888
$ python app.py
```

