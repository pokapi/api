import flask
from flask import request, jsonify
from flask_api import status
from flask_cors import CORS
import sys
sys.path.append('./data/')
from pokemons_min import pokemons_min
from pokemons_max import pokemons_max

app = flask.Flask(__name__)
app.config["DEBUG"] = True
CORS(app, support_credentials=True)

@app.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"


@app.route('/api/pokemon/all', methods=['GET'])
def api_all():
    return jsonify(pokemons_min)


@app.route('/api/pokemon', methods=['GET'])
def api_name():
    # Create buffer
    results = []

    # Si il y a 'name' dans les arguments(GET)
    # La variable 'name' prend la valeur de l'argument
    if 'name' in request.args:
        pokemonName = request.args['name'].lower().capitalize()

        # Cherche dans la liste de pokemon si il y a une correspondance
        for pokemon in pokemons_max:
            if pokemon['name'] == pokemonName:
                results = pokemon

    # Si il y a 'type' dans les arguments(GET)
    # La variable 'type' prend la valeur de l'argument
    elif 'type' in request.args:
        pokemonType = request.args['type'].lower().capitalize()

        # Cherche dans la liste de pokemon si il y a une correspondance
        for pokemon in pokemons_min:
            if pokemonType in pokemon['types']:
                results.append(pokemon)

    # Si il y a 'id' dans les arguments(GET)
    # La variable 'id' prend la valeur de l'argument
    elif 'id' in request.args:
        pokemonId = request.args['id']

        # Cherche dans la liste de pokemon si il y a une correspondance
        for pokemon in pokemons_max:
            if str(pokemon['id']) == str(pokemonId):
                results = pokemon


    # La fonction jsonify de Flask convertit notre dictionnaire au format JSON
    return jsonify(results) if results != [] else ('Erreur 404', status.HTTP_404_NOT_FOUND)

@app.after_request
def creds(response):
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return response

app.run(host='localhost', port=8888)
